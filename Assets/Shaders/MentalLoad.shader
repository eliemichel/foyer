﻿Shader "Unlit/MentalLoad"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
		_RedColor("Red", Color) = (1, 0, 0, 1)
		_GreenColor("Green", Color) = (0, 1, 0, 1)
		_Value("Value", Range(0, 1)) = 0.5
		_Increasing("Increasing", Range(0, 1)) = 0.0
		_Decreasing("Decreasing", Range(0, 1)) = 0.0
		_ForegroundColor("ForegroundColor", Color) = (0.3, 0.3, 0.3, 1)
		_BackgroundColor("BackgroundColor", Color) = (0.9, 0.9, 0.9, 1)
		_CharacterColor("CharacterColor", Color) = (0.1, 0.6, 0.9, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _RedColor;
			float4 _GreenColor;
			float _Value;
			float _Increasing;
			float _Decreasing;
			float4 _ForegroundColor;
			float4 _BackgroundColor;
			fixed4 _CharacterColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float4 red = _RedColor;
				float4 green = _GreenColor;
				float redArrow = sin((i.uv.x + abs(i.uv.y - 0.5) * 0.1) * 50 - _Time.y * 10.0);
				float greenArrow = sin((i.uv.x - abs(i.uv.y - 0.5) * 0.1) * 50 + _Time.y * 10.0);
				red += step(redArrow, -0.5) * 0.5;
				green += step(greenArrow, -0.5) * 0.5;

				fixed4 col = red * step(i.uv.x, _Value) * _Increasing + green * (1 - step(i.uv.x, _Value)) * _Decreasing;

				col = fixed4(0, 0, 0, 1);
				col += lerp(_ForegroundColor, _BackgroundColor, step(i.uv.x, _Value));

				col = lerp(col, red, step(i.uv.x, _Value) * _Increasing);
				//col = lerp(col, green, (1 - step(i.uv.x, _Value)) * _Decreasing);
				col = lerp(col, green, step(i.uv.x, _Value) * _Decreasing);

				if (_Value >= 1 && _Decreasing < 1) {
					col = _RedColor * step(sin(_Time.y * 10.0), 0.0);
				}

				col = lerp(col, _CharacterColor, step(i.uv.y, 0.2));

				col.a = 1.0;

                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
