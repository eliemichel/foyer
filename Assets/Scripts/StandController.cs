﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static TaskManager;

public class StandController : MonoBehaviour
{
    public Task task;

    private enum State
    {
        Rest,
        Needed,
        Assigned,
        Completed,
    }
    private State state;
    
    public float completeSpeed = 0.2f;
    public float completed = 0.0f;
    public float needSpeed = 0.1f;
    public float need = 0.0f;

    public float minDelay = 3;
    public float maxDelay = 6;

    public float radius = 1.0f;
    private CharacterController user;

    public StandHudController hudPrefab;
    private StandHudController hud;
    public Canvas hudCanvas;
    public Renderer skin;

    private RoomController room;

    public bool IsNeeded()
    {
        return state == State.Needed;
    }
    public bool IsCompleted()
    {
        return state != State.Assigned;
    }

    public void SetUser(CharacterController character)
    {
        user = character;
        state = user != null ? State.Assigned : State.Completed;
    }
    
    private void Start()
    {
        room = GetComponentInParent<RoomController>();
        state = State.Completed;

        hud = Instantiate(hudPrefab, hudCanvas.transform);
        hud.task = task;
        hud.GetComponent<FollowHud>().target = skin;
    }

    private void StartNeed()
    {
        if (state == State.Rest)
        {
            state = State.Needed;
        }
    }

    private void Update()
    {
        switch (state)
        {
            case State.Completed:
                completed = 0.0f;
                need = 0.0f;
                state = State.Rest;
                float delay = Random.Range(minDelay, maxDelay);
                Invoke("StartNeed", delay);
                break;

            case State.Needed:
                need += needSpeed * Time.deltaTime;
                CharacterController character = room.PickCharacter(task, need);
                if (character != null)
                {
                    character.SetTargetStand(this);
                    state = State.Assigned;
                }
                break;

            case State.Assigned:
                Vector3 toUser = user.transform.position - transform.position;
                if (toUser.magnitude < radius)
                {
                    completed += completeSpeed * Time.deltaTime;
                    if (completed >= 1)
                    {
                        user.UnsetTargetStand();
                        state = State.Completed;
                    }
                }
                break;
        }

        hud.showWarning = false;// state == State.Needed || state == State.Assigned;
        hud.gaugeLevel = need;
    }
}
