﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudController : MonoBehaviour
{
    protected void UpdateGauge(Image background, Image gauge, float gaugeLevel)
    {
        Vector2 s = gauge.rectTransform.sizeDelta;
        s.x = background.rectTransform.sizeDelta.x * gaugeLevel;
        gauge.rectTransform.sizeDelta = s;

        Vector3 p = background.rectTransform.anchoredPosition;
        p.x = p.x - background.rectTransform.sizeDelta.x / 2 + s.x / 2;
        gauge.rectTransform.anchoredPosition = p;
    }
}
