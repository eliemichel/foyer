﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Picker : MonoBehaviour
{
    public struct RoomHit
    {
        public RoomController room;
        public Vector3 position;
    }

    public float draggedCharacterUpOffset = -0.5f;

    public GameObject targetPrefab;
    private GameObject currentTarget;

    private AbstractCharacterController draggedCharacter;
    private RoomController targetRoom;
    private Vector3 targetPosition;

    void Update()
    {
        AbstractCharacterController character = GetHoveredCharacter();
        if (character != null)
        {
            character.Highlight();
            if (Input.GetMouseButtonDown(0))
            {
                StartDrag(character);
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            StopDrag();
        }

        if (IsDragging())
        {
            Vector3 dragPos = DragPosition();
            if (dragPos.y >= -100) // si la position est valide
            {
                RoomHit hit = RoomUnder(dragPos);
                if (hit.room != null)
                {
                    targetRoom = hit.room;
                    targetPosition = hit.position;

                    if (currentTarget == null)
                    {
                        currentTarget = Instantiate(targetPrefab, targetPosition, Quaternion.identity);
                    }
                    currentTarget.transform.position = targetPosition;
                }

                draggedCharacter.transform.position = dragPos + Vector3.up * draggedCharacterUpOffset;
            }
        }
    }

    private bool IsDragging()
    {
        return draggedCharacter != null;
    }

    private Vector3 DragPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        int layerMask = 1 << 10; // DragArea

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            return hit.point;
        }

        return new Vector3(0, -1000, 0); // position considérée comme invailde
    }

    public static RoomHit RoomUnder(Vector3 pos)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        int layerMask = 1 << 11; // Room

        RoomHit roomHit = new RoomHit();
        roomHit.room = null;

        if (Physics.Raycast(pos, Vector3.down, out hit, Mathf.Infinity, layerMask))
        {

            roomHit.room = hit.transform.GetComponent<RoomController>();
            roomHit.position = hit.point;
        }

        return roomHit;
    }

    AbstractCharacterController GetHoveredCharacter()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        int layerMask = 1 << 9; // Draggable

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            return hit.transform.GetComponent<AbstractCharacterController>();
        }

        return null;
    }
    
    void StartDrag(AbstractCharacterController character)
    {
        draggedCharacter = character;
        draggedCharacter.Pause();
        targetRoom = draggedCharacter.currentRoom;
    }

    void StopDrag()
    {
        if (!IsDragging())
        {
            return;
        }

        draggedCharacter.ChangeRoom(targetRoom);
        draggedCharacter.transform.position = targetPosition;
        draggedCharacter.Unpause();
        draggedCharacter = null;

        if (currentTarget != null)
        {
            Destroy(currentTarget);
        }
    }
}
