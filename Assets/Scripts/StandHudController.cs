﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StandHudController : HudController
{
    public float gaugeLevel;
    public Image[] warningSigns;
    public bool showWarning = false;
    public float freq = 1.0f;
    public TaskManager.Task task;
    
    private void Update()
    {
        // UpdateGauge(background, gauge, gaugeLevel);
        bool c = Mathf.Sin(Time.timeSinceLevelLoad * 2 * Mathf.PI * freq) > 0;

        for (int i = 0; i < warningSigns.Length; ++i)
        {
            warningSigns[i].sprite = TaskManager.Sprite(task);
            warningSigns[i].enabled = i < gaugeLevel && c;
        }
    }
}
