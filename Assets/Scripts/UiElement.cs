﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UiElement : MonoBehaviour
{
    public Renderer target;
    public float scale = 1.0f;

    void Update()
    {
        transform.rotation = Camera.main.transform.rotation;
        float s = scale / Vector3.Distance(Camera.main.transform.position, transform.position); // not working
        transform.localScale = new Vector3(s, s, s);
    }
}
