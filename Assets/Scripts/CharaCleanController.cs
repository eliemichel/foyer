﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharaCleanController : AbstractCharacterController
{
    public Transform target;
    public Animator anim;

    private Vector3 holdPos;
    private int holdCount;
    private float holdThreshold = 0.1f;
    public int holdLimit = 60;
    public bool isStopped = false;

    new void Start()
    {
        base.Start();
    }

    new void Update()
    {
        agent.destination = target.position;

        bool isHome = Vector3.Distance(target.position, transform.position) < 0.5f;
        anim.SetBool("isWalking", !isHome && !isStopped);
        
        Picker.RoomHit hit = Picker.RoomUnder(target.position + new Vector3(0, 100, 0));
        bool isTargetInWrongRoom = hit.room != currentRoom;

        if (!isHome && Vector3.Distance(transform.position, holdPos) < holdThreshold)
        {
            holdCount++;
        }
        else
        {
            holdPos = transform.position;
            holdCount = 0;
        }
        isStopped = holdCount > holdLimit;

        if (isHome || isTargetInWrongRoom || isStopped)
        {
            FindNextMove();
        }
        base.Update();
    }

    void FindNextMove()
    {
        Vector3 blob = currentRoom.FindResentmentBlob();
        if (blob.y < -100)
        {
            return;
        }
        float lambda = 100;
        float dlambda = 50;
        Picker.RoomHit hit = new Picker.RoomHit();
        for (int i = 0; i < 20; ++i)
        {
            Vector3 pos = transform.position + (blob - transform.position) * lambda;
            hit = Picker.RoomUnder(pos + new Vector3(0, 100, 0));
            if (hit.room != currentRoom)
            {
                lambda -= dlambda;
            }
            else
            {
                lambda += dlambda;
            }
            dlambda /= 2;
        }
        target.position = hit.position;
        holdCount = 0;
    }
}
