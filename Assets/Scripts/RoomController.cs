﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomController : MonoBehaviour
{
    public StandController[] stands;
    public Transform sofa;

    public ResentmentController resentmentPrefab;

    private Dictionary<Color, List<ResentmentController>> resentments;
    private HashSet<CharacterController> characters;

    private void Awake()
    {
        resentments = new Dictionary<Color, List<ResentmentController>>();
        characters = new HashSet<CharacterController>();
    }

    public void AddCharacter(CharacterController character)
    {
        characters.Add(character);
    }
    public void RemoveCharacter(CharacterController character)
    {
        characters.Remove(character);
    }

    public Vector3 FindResentmentBlob()
    {
        List<Vector3> positions = new List<Vector3>();
        foreach (var k in resentments.Keys)
        {
            HashSet<ResentmentController> toClean = new HashSet<ResentmentController>();
            foreach (var r in resentments[k])
            {
                if (r.WillDie)
                {
                    toClean.Add(r);
                }
                else
                {
                    positions.Add(r.transform.position);
                }
            }
            foreach (var r in toClean)
            {
                resentments[k].Remove(r);
            }
        }
        
        if (positions.Count == 0)
        {
            return new Vector3(0, -1000, 0);
        }
        int i = Random.Range(0, positions.Count);
        return positions[i];
    }

    public CharacterController PickCharacter(TaskManager.Task task, float need)
    {
        float min = -1;
        int count = 0;
        foreach (CharacterController c in characters)
        {
            if (c.IsBusy())
            {
                continue;
            }
            float th = c.threshold[(int)task];
            if (min < 0 || th < min)
            {
                min = th;
                count = 1;
            }
            else if (th == min)
            {
                count++;
            }
        }

        if (min > need + 1) // le +1 est un dirty dirty bugfix
        {
            // aucun candidat
            return null;
        }

        int i = Random.Range(0, count);
        int j = 0;
        foreach (CharacterController c in characters)
        {
            float th = c.threshold[(int)task];
            if (th > min || c.IsBusy())
            {
                continue;
            }
            if (j == i)
            {
                return c;
            }
            ++j;
        }
        return null; // assert false
    }

    // Retourne une couleur qui n'est pas déjà dans la piece
    public Color NewColor()
    {
        // TODO: prendre dans une liste de couleurs predefinies
        return Color.HSVToRGB(Random.Range(0, 1), 1, 1);
    }

    public void AddResentment(CharacterController character)
    {
        Vector3 p = character.transform.position;
        p.y += 0.5f;
        float theta = Random.Range(0.0f, Mathf.PI);
        p.x += 0.5f * Mathf.Cos(theta);
        p.z += 0.5f * Mathf.Sin(theta);
        ResentmentController resentment = Instantiate(resentmentPrefab, p, Quaternion.identity);
        resentment.SetColor(character.color);
        if (!resentments.ContainsKey(character.color))
        {
            resentments[character.color] = new List<ResentmentController>();
        }
        resentments[character.color].Add(resentment);

        CleanResentment();
    }

    private void CleanResentment()
    {
        // Si un resentiment de chaque personne est présent, les retirer, ils s'équilibrent
        int count = -1;
        foreach (CharacterController c in characters)
        {
            if (!resentments.ContainsKey(c.color))
            {
                count = 0;
            }
            else if (count == -1)
            {
                count = resentments[c.color].Count;
            }
            else
            {
                count = Mathf.Min(count, resentments[c.color].Count);
            }
        }

        for (int i = 0; i < count; ++i)
        {
            foreach (Color c in resentments.Keys)
            {
                if (resentments[c].Count == 0)
                {
                    continue;
                }
                resentments[c][0].Kill();
                resentments[c].RemoveAt(0);
            }
        }
    }
}
