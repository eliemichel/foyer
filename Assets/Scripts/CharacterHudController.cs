﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterHudController : HudController
{
    public float cleaningGaugeLevel;
    public float washingDishesGaugeLevel;

    public Image[] washingDots;
    public Image[] cleaningDots;
    
    public CharacterController character;

    private Renderer mentalLoad;

    private void Start()
    {
        Sprite sprite = TaskManager.Sprite(TaskManager.Task.Cleaning);
        foreach (Image dot in cleaningDots)
        {
            dot.color = new Color(character.color.r, character.color.g, character.color.b);
            dot.sprite = sprite;
        }

        sprite = TaskManager.Sprite(TaskManager.Task.WashingDishes);
        foreach (Image dot in washingDots)
        {
            dot.color = new Color(character.color.r, character.color.g, character.color.b);
            dot.sprite = sprite;
        }

        mentalLoad = character.mentalLoadHud;
        Material mat = Instantiate(mentalLoad.material);
        mentalLoad.material = mat;
        mentalLoad.material.SetColor("_CharacterColor", character.color);
    }

    private void Update()
    {
        //UpdateGauge(cleaningBackground, cleaningGauge, cleaningGaugeLevel);
        for (int i = 0; i < washingDots.Length; ++i)
        {
            washingDots[i].enabled = i < washingDishesGaugeLevel;
        }
        for (int i = 0; i < cleaningDots.Length; ++i)
        {
            cleaningDots[i].enabled = i < cleaningGaugeLevel;
        }

        mentalLoad.material.SetFloat("_Value", character.mentalLoadGauge);
        mentalLoad.material.SetFloat("_Decreasing", character.OnSofa() && !character.IsBusy() ? 1 : 0);
        mentalLoad.material.SetFloat("_Increasing", character.IsBusy() ? 1 : 0);
    }
}
