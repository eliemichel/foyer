﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResentmentController : MonoBehaviour
{
    public Color color;

    private bool willDie = false;
    public bool WillDie { get { return willDie; } }

    public void Kill()
    {
        if (willDie)
        {
            return;
        }
        willDie = true;

        //SetColor(new Color(1, 1, 1));
        Destroy(GetComponent<Rigidbody>());
        Invoke("DestroyParent", 1.0f);
    }

    private void DestroyParent()
    {
        Destroy(gameObject);
    }

    private void Update()
    {
        if (willDie)
        {
            Vector3 p = transform.position;
            p.y += 3.0f * Time.deltaTime;
            transform.position = p;
        }

        if (transform.position.y < -100)
        {
            Kill();
        }
    }

    public void SetColor(Color newColor)
    {
        Renderer r = GetComponent<Renderer>();
        color = newColor;
        r.material.SetColor("_Color", color);
    }


}
