﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskManager : MonoBehaviour
{
    public enum Task
    {
        Cleaning,
        WashingDishes,
        Market
    }

    public Sprite[] sprites = new Sprite[] { };
    public Color[] colors = new Color[] {
        new Color(0.145f, 0.514f, 0.773f),
        new Color(0.6f, 0.137f, 0.0f),
        new Color(0, 0, 1)
    };

    public static Sprite Sprite(Task task)
    {
        return instance.sprites[(int)task];
    }

    // obsolete
    public static Color Color(Task task)
    {
        return instance.colors[(int)task];
    }

    // Singleton managment

    private static TaskManager s_Instance = null;
    public static TaskManager instance
    {
        get
        {
            if (s_Instance == null)
            {
                // FindObjectOfType() returns the first TaskManager object in the scene.
                s_Instance = FindObjectOfType(typeof(TaskManager)) as TaskManager;
            }

            // If it is still null, create a new instance
            if (s_Instance == null)
            {
                var obj = new GameObject("TaskManager");
                s_Instance = obj.AddComponent<TaskManager>();
            }

            return s_Instance;
        }
    }

    void OnApplicationQuit()
    {
        s_Instance = null;
    }
}
