﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FollowHud : MonoBehaviour
{
    public Renderer target;
    public float margin = 0.1f;

    private void Update()
    {
        Vector3 p = target.transform.position;
        p.y = target.bounds.max.y + margin;
        Vector3 uv = Camera.main.WorldToScreenPoint(p);
        transform.position = uv;
    }
}
