﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AbstractCharacterController : MonoBehaviour
{
    public RoomController currentRoom;

    protected NavMeshAgent agent;

    public Color color;
    public Renderer[] skins;

    protected bool isPaused = false;
    public void Pause()
    {
        isPaused = true;
        agent.isStopped = true;
    }
    public void Unpause()
    {
        isPaused = false;
        agent.Warp(transform.position);
        agent.isStopped = false;
    }

    public virtual void ChangeRoom(RoomController room)
    {
        if (room == currentRoom)
        {
            return;
        }
        
        currentRoom = room;
    }

    public void Highlight()
    {
        foreach (Renderer s in skins)
        {
            s.material.SetFloat("_Outline", 0.003f);
            s.material.SetColor("_OutlineColor", color);
        }
    }

    protected void Start()
    {
        currentRoom = GetComponentInParent<RoomController>();
        agent = GetComponent<NavMeshAgent>();
    }

    protected void Update()
    {
        // Fade out outline
        foreach (Renderer s in skins)
        {
            float outline = s.material.GetFloat("_Outline"); ;
            s.material.SetFloat("_Outline", outline / 2);
        }
    }
}
