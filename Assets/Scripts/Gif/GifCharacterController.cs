﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GifCharacterController : MonoBehaviour
{
    public float speed = 1.0f;
    public Color color;
    public float sign = 1;
    public ResentmentController resentmentPrefab;

    void Start()
    {
        InvokeRepeating("SpawnCube", 0.5f, 1.0f);
    }

    private void Update()
    {
        Vector3 p = transform.position;
        p.z += sign * speed * Time.deltaTime;
        transform.position = p;
    }

    void SpawnCube()
    {
        Quaternion q = Quaternion.Euler(Random.Range(0, 180), Random.Range(0, 180), Random.Range(0, 180));
        ResentmentController r = Instantiate(resentmentPrefab, transform.position + new Vector3(0, 0.5f, -0.5f * sign), q);
        r.SetColor(color);
        Rigidbody rb = r.GetComponent<Rigidbody>();
        rb.velocity = new Vector3(Random.Range(-0.5f, 0.5f), 1.0f, sign > 0 ? Random.Range(-0.5f, 0) : Random.Range(0, 0.5f));
    }
}
