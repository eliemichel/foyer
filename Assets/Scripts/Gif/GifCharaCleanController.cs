﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GifCharaCleanController : MonoBehaviour
{
    public float speed = 1.0f;
    private bool hasStarted = false;

    private void Start()
    {
        Invoke("StartMove", 13);
    }

    void StartMove()
    {
        hasStarted = true;
    }

    private void Update()
    {
        if (hasStarted)
        {
            Vector3 p = transform.position;
            p.x -= speed * Time.deltaTime;
            transform.position = p;
        }
    }
}
