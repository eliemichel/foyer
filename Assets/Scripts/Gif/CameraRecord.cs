﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;

public class CameraRecord : MonoBehaviour
{
    public int nbFrames = -1; // number of frames to capture
    public int fps = 60;

    Camera cam;

    private void Start()
    {
        cam = GetComponent<Camera>();
        Application.targetFrameRate = fps;
        QualitySettings.vSyncCount = 0;
    }

    void OnPostRender()
    {
        if (nbFrames < 0 || Time.frameCount < nbFrames)
        {
            Texture2D tex = new Texture2D(Screen.width, Screen.height);
            RenderTexture.active = cam.targetTexture;
            tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            tex.Apply();
            byte[] data = tex.EncodeToPNG();
            Destroy(tex);
            File.WriteAllBytes(Application.dataPath + String.Format("/../Render/{0:D04}.png", Time.frameCount), data);
        }
    }
}
