﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterController : AbstractCharacterController
{
    public float[] threshold = new float[3]{
        1.0f, 1.0f, 1.0f
    };

    public float speed = 1.0f;

    private StandController targetStand;

    public CharacterHudController hudPrefab;
    public Canvas hudCanvas;
    public Renderer mentalLoadHud;

    public float mentalLoadGauge = 0.1f;
    public float mentalLoadGaugeIncreaseSpeed = 0.2f;
    public float mentalLoadGaugeDecreaseSpeed = 0.1f;

    private float isOnSofa = 0;
    
    public void SetTargetStand(StandController stand)
    {
        targetStand = stand;
        stand.SetUser(this);
        agent.destination = targetStand.transform.position;

        // Emettre du resentiment
        currentRoom.AddResentment(this);

    }
    public void UnsetTargetStand()
    {
        targetStand.SetUser(null);
        targetStand = null;
    }

    public bool IsBusy()
    {
        return targetStand != null;
    }

    public override void ChangeRoom(RoomController room)
    {
        if (room == currentRoom)
        {
            return;
        }

        if (IsBusy())
        {
            UnsetTargetStand();
        }

        currentRoom.RemoveCharacter(this);
        currentRoom = room;

        // Nouvelle couleur
        // TODO: restore ? seulement s'il y a trop de personnages, plus que de couleurs possibles
        //color = currentRoom.NewColor();

        currentRoom.AddCharacter(this);
    }

    new void Start()
    {
        base.Start();
        currentRoom.AddCharacter(this);

        CharacterHudController hud = Instantiate(hudPrefab, hudCanvas.transform);
        hud.character = this;
        hud.cleaningGaugeLevel = threshold[(int)TaskManager.Task.Cleaning];
        hud.washingDishesGaugeLevel = threshold[(int)TaskManager.Task.WashingDishes];
        hud.GetComponent<FollowHud>().target = skins[0];
    }

    public bool OnSofa()
    {
        return isOnSofa > 0.2;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "SofaArea")
        {
            isOnSofa = 1;
        }
    }

    new void Update()
    {
        if (isPaused)
        {
            isOnSofa = 0;
            return;
        }
        
        if (IsBusy())
        {
            agent.destination = targetStand.transform.position;
            mentalLoadGauge = Mathf.Max(mentalLoadGauge, 0.1f);
            mentalLoadGauge += mentalLoadGaugeIncreaseSpeed * Time.deltaTime;
        }
        else
        {
            // S'il n'y a aucun stand qui a besoin d'aide, aller dans la zone d'idle (le sofa)
            agent.destination = currentRoom.sofa.transform.position;

            if (OnSofa())
            {
                mentalLoadGauge -= mentalLoadGaugeDecreaseSpeed * Time.deltaTime;
            }
        }

        mentalLoadGauge = Mathf.Clamp(0, mentalLoadGauge, 1);

        base.Update();

        isOnSofa /= 2;
    }

}
